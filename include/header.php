<!-- header section -->
<header class="xs-header header-transparent">
	<div class="container">
		<nav class="xs-menus">
			<div class="nav-header name">
				<div class="nav-toggle"></div>
				<a href="index.php" class="nav-logo">
					<img src="assets/images/logo-meen.png" alt="">
					<span class="span">Meenseh</span>
					<span >Sansthan</span>
				</a>
			</div><!-- .nav-header END -->
			<div class="nav-menus-wrapper row">
				<div class="xs-logo-wraper col-lg-2 xs-padding-0">
					<a class="nav-brand" href="index.php">
						<img style="width: 120px;margin-top: 5px;" src="assets/images/logo-meen.png" alt="">
					</a>
				</div><!-- .xs-logo-wraper END -->
				<div class="col-lg-7">
					<ul class="nav-menu">
						<li><a href="index.php">Home</a></li>
						<li><a href="about.php">about</a></li>
						<li><a href="causes.php">Causes</a></li>
						<li><a href="#">Events</a>
							<ul class="nav-dropdown">
								<li><a href="event.php">Event</a></li>
								<li><a href="event-single.php">Event single</a></li>
							</ul>
						</li>
						<li><a href="#">Blog</a>
							<ul class="nav-dropdown">
								<li><a href="blog.php">Blog</a></li>
								<li><a href="blog-single.php">Blog single</a></li>
							</ul>
						</li>
						<li><a href="#">Pages</a>
							<ul class="nav-dropdown">
								<li><a href="donate-now.html">donate now</a></li>
								<li><a href="faq.html">FAQ</a></li>
								<li><a href="mission.html">mission</a></li>
								<li><a href="portfolio.html">portfolio</a></li>
								<li><a href="team.html">team</a></li>
								<li><a href="volunteer.html">volunteer</a></li>
							</ul>
						</li>
						<li><a href="contact.php">Contact</a></li>
					</ul><!-- .nav-menu END -->
				</div>
				<div class="xs-navs-button d-flex-center-end col-lg-3">
					<a href="#" class="btn btn-primary">
						<span class="badge"><i class="fa fa-heart"></i></span> Contribute Now
					</a>
				</div><!-- .xs-navs-button END -->
			</div><!-- .nav-menus-wrapper .row END -->
		</nav><!-- .xs-menus .fundpress-menu END -->
	</div><!-- .container end -->
</header><!-- End header section -->